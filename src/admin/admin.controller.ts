import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
  HttpException,
  UseInterceptors,
  CacheInterceptor,
  CacheTTL,
} from '@nestjs/common';
import { Response } from 'express';
import { AdminService } from './admin.service';
import { CreateAdminDto } from './dto/create-admin.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';

@UseInterceptors(CacheInterceptor)
@Controller()
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @Post()
  async create(@Body() createAdminDto: CreateAdminDto, @Res() res: Response) {
    try {
      await this.adminService.create(createAdminDto);
      return res
        .status(HttpStatus.CREATED)
        .jsonp({ eStatus: HttpStatus.CREATED, oData: createAdminDto });
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'Internal server error',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get()
  @CacheTTL(5)
  findAll() {
    return this.adminService.findAll();
  }

  @Get(':id')
  // @Render('index.hbs')
  async findOne(@Param('id') id: string, @Res() res: Response) {
    const data = await this.adminService.findOne(id);
    return res.render('index.hbs', data);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAdminDto: UpdateAdminDto) {
    return this.adminService.update(id, updateAdminDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.adminService.remove(id);
  }
}
