import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Admin, AdminDocument } from './entities/admin.entity';
import { CreateAdminDto } from './dto/create-admin.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';
const { ObjectId } = Types;

@Injectable()
export class AdminService {
  constructor(
    @InjectModel(Admin.name) private AdminModel: Model<AdminDocument>,
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
  ) {}

  async create(createAdminDto: CreateAdminDto): Promise<Admin> {
    console.log('createAdminDto :: ', createAdminDto);
    return this.AdminModel.create(createAdminDto);
  }

  async findAll(): Promise<Admin[]> {
    // await this.cacheManager.set('key1', 'hello!', { ttl: 5 });
    // const cached = await this.cacheManager.get('key1');
    // console.log(cached);
    return this.AdminModel.find({}).lean();
  }

  async findOne(id: string): Promise<Admin> {
    return this.AdminModel.findOne({ _id: new ObjectId(id) }).lean();
  }

  async update(id: string, updateAdminDto: UpdateAdminDto): Promise<any> {
    console.log('updateAdminDto :: ', updateAdminDto);
    return this.AdminModel.updateOne({ _id: new ObjectId(id) }, updateAdminDto);
  }

  async remove(id: string): Promise<any> {
    return this.AdminModel.deleteOne({ _id: new ObjectId(id) });
  }
}
