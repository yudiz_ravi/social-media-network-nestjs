import { Exclude } from 'class-transformer';
import {
  IsString,
  Length,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  Matches,
} from 'class-validator';

export class CreateAdminDto {
  @IsString()
  @IsNotEmpty()
  sName: string;

  @IsString()
  @IsNotEmpty()
  @Length(10, 10)
  sMobile: string;

  @IsEmail()
  @IsNotEmpty()
  sEmail: string;

  @IsString()
  @IsNotEmpty()
  sUsername: string;

  @IsString()
  @IsNotEmpty()
  @Length(8)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message:
      'Passwords will contain at least 1 upper case letter, at least 1 lower case letter, at least 1 number or special character',
  })
  @Exclude()
  sPassword: string;

  @IsString()
  @IsOptional()
  sProPic: string;

  constructor(partial: Partial<CreateAdminDto>) {
    Object.assign(this, partial);
  }
}
