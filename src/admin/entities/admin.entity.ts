import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type AdminDocument = Admin & Document;

@Schema()
export class Admin {
  @Prop({ required: true, trim: true })
  sName: string;

  @Prop({ required: true, trim: true })
  sFullName: string;

  @Prop({ required: true, trim: true, unique: true })
  sUsername: string;

  @Prop({ required: true, trim: true, unique: true })
  sEmail: string;

  @Prop({ required: true })
  sMobile: string;

  @Prop({ required: true })
  sPassword: string;

  @Prop({ trim: true })
  sProPic: string;

  @Prop({ enum: ['Y', 'B', 'D'], default: 'Y' })
  eStatus: string;
}

export const AdminSchema = SchemaFactory.createForClass(Admin);
