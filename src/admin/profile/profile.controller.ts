import {
  Controller,
  Get,
  ClassSerializerInterceptor,
  UseInterceptors,
  UseGuards,
} from '@nestjs/common';
import { RolesGuard } from 'src/guards/role.guard';
import { CreateAdminDto } from '../dto/create-admin.dto';
import { ProfileService } from './profile.service';

@UseGuards(RolesGuard)
@Controller()
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Get()
  getProfile(): Promise<CreateAdminDto[]> {
    return this.profileService.findAllProfile();
  }
}
