import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Admin, AdminDocument } from '../entities/admin.entity';

@Injectable()
export class ProfileService {
  constructor(
    @InjectModel(Admin.name) private readonly AdminModel: Model<AdminDocument>,
  ) {}

  async findAllProfile(): Promise<Admin[]> {
    return this.AdminModel.find({}).lean();
  }
}
