import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
// import { Roles } from '../enum.dto';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  roles = ['likes', 'comments', 'share', 'post'];

  validateRole(role: string): boolean {
    return this.roles.includes(role);
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    // const roles = this.reflector.get<string[]>('roles', context.getHandler());
    const roles = this.roles;
    if (!roles) return true;
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    return this.validateRole(user.roles);
  }
}
