import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { RequestService } from '../request.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly requestService: RequestService) {}
  use(req: Request, res: Response, next: NextFunction) {
    const token = req.header('Authorization');
    this.requestService.verifyUser(token);
    next();
  }
}
