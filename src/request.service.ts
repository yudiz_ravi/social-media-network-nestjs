import { Injectable, Scope } from '@nestjs/common';

@Injectable({ scope: Scope.REQUEST })
export class RequestService {
  private userId: string;

  storeUser(userId: string) {
    this.userId = userId;
  }

  verifyUser(token: string) {
    this.userId = token; // jwt.verify(token)
    return this.userId;
  }

  authorizeAdmin() {
    return this.userId;
  }
}
