import { Controller, Get, HttpCode, Param, Query } from '@nestjs/common';
import { UsersService as UserService } from './users.service';
// import { Request, Response } from 'express';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UserService) {}

  @Get('profile')
  @HttpCode(400)
  getProfile(): string {
    return this.userService.getProfile();
  }

  @Get('/:id/breakup/:pid')
  findOne(@Param('id') id: string, @Param('pid') pid: string): string {
    return `This action returns a ${id}-${pid} cat`;
  }

  @Get('default')
  async findAll(@Query() query): Promise<any[]> {
    console.log(`you have passed query is ${query.name}-${query.id}.`);
    return [];
  }
}
